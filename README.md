# web-project-template #

This is a node.js based repo designed to be a quick starting point for any web development project.
It differs from my other starter template [web-experiment-template](https://bitbucket.org/sortofsleepy/web-experiment-template) in that this template includes css pre-processing.

It's meant to provide a minimal set of tools for building websites and includes -  

* [Browserify](http://browserify.org/)
* [Gulp](http://gulpjs.com/)
* [PostCSS](https://github.com/postcss/postcss)
* [PreCss(for using Scss style css)](https://github.com/jonathantneal/precss)

It also supports ES6/ES2015 style Javascript with [BabelJS](https://babeljs.io/) 

### How do I get set up? ###

* `npm install`
* `gulp`
* A new browser window should load up.

### Notes ###
* Source files are meant to go in the `src` directory. From there they get compiled and put into the `public` directory which is what you can then take and dump on a webserver somewhere. 

* CSS source files are meant to go into the `css` directory inside the root. From there, the styles get compiled into the public directory, also under a folder called `css` (yes yes I know this probably ought to change)