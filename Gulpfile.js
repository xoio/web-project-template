
var watchify = require('watchify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var assign = require('lodash.assign');
var babelify = require('babelify');
var concat = require("gulp-concat");
var postcss = require('postcss');

// add custom browserify options here
var customOpts = {
    entries: ['./src/index.js'],
    debug: true,
    transform:babelify
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts));
var browserSync = require('browser-sync');

// add transformations here
// i.e. b.transform(coffeeify);
b.transform(babelify);

gulp.task('js', bundle); // so you can run `gulp js` to build the file
b.on('update', bundle); // on any dep update, runs the bundler
b.on('log', gutil.log); // output build logs to terminal

function bundle() {
    browserSync.reload();
    return b.bundle()
        // log errors if they happen
        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
        .pipe(source('bundle.js'))
        // optional, remove if you don't need to buffer file contents
        .pipe(buffer())
        // optional, remove if you dont want sourcemaps
        .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
        // Add transformation tasks to the pipeline here.
        .pipe(sourcemaps.write('./')) // writes .map file
        .pipe(gulp.dest('./public/js'));
}
gulp.task("libs",function(){
    return gulp.src('./libs/*.js')
        .pipe(concat("libs.js"))
        //.pipe(compress())
        .pipe(gulp.dest('./public/js/libs'))
});

gulp.task('css', function () {
    var postcss    = require('gulp-postcss');
    return gulp.src('./css/main.css')
        .pipe( sourcemaps.init() )
        .pipe( postcss([ require('precss') ]) )
        .pipe( sourcemaps.write('.') )
        .pipe( gulp.dest('./public/css') );
});
gulp.task("default",["js","libs","css"],function(){
    browserSync({
        server:{baseDir:"./public"}
    });
    gulp.watch(["./css/main.css"],["css"]);
    gulp.watch(["./css/**/*.scss"],["css"]);
    gulp.watch(["./src/**/*.js"],["js"]);
});